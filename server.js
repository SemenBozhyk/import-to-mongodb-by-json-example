var express = require('express');
var app = express();
var path = require('path');
var fs = require('fs');
var bodyParser = require('body-parser');

global.__base = __dirname + '/';

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./api/admin/routes/import_1c');
routes(app);

app.listen(3000, "0.0.0.0", function () {
  console.log('im here with 3000')
});
