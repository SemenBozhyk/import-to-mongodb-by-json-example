exports.config = require('./config').json;

exports.init_catalog = function (db, server) {

  const self           = this,
        catalogContent = db.collection(self.config.catalogCollection),
        catalogValues  = db.collection(self.config.catalogValuesCollection);

  return catalogContent.find({'_id': 'catalog'}).toArray()
    .then(docs => {

      if (docs.length == 0) {

        return catalogContent.insertOne(self.config.catalogModel)
          .then(() => server.write('Экземпляр хранилища каталога создан\n'))
          .then(() => self.value_serializer({
            self: self,
            mode: 'values',
            values: self.config.initValues,
            update: [],
            server: server,
            colCount: 0
          }))
          .then((initArray) => catalogValues.insertMany(initArray[1]))
          .then(() => server.write('Дефолтные свойства созданы (' + self.config.initValues.length + ')\n'))
          .catch((err) => {
            throw err
          })

      } else {
        return server.write('Выбранный каталог существует\n')
      }

    })
    .catch((err) => {
      throw err
    })
};

exports.set = function (db, mode, valuesArray, server) {

  const self    = this,
        errMsg  = 'В файле поле ' + mode + ' пусто. Такого не должно происходить',
        colName = self.config['catalog' + mode.charAt(0).toUpperCase() + mode.slice(1) + 'Collection'],
        values  = db.collection(colName);

  let idsToFind = [],
      colCount;

  server.write('Обработка поступивших ' + mode + ' (' + valuesArray.length + ')' + '\n');

  if (valuesArray.length > 0) {

    valuesArray.forEach(oneValue => idsToFind.push(oneValue.id));

    let actionArray,
        promiseStore = [];

    return values.count({})
      .then(count => {
        colCount = count;
        return values.find({'_id': {$in: idsToFind}}).toArray()
      })
      .then(docs => {
        return self.value_serializer({
          self: self,
          mode: mode,
          values: valuesArray,
          update: docs,
          server: server,
          colCount: (colCount) ? colCount : 0
        })
      })
      .then((serializedArray) => {
        actionArray = serializedArray;
        if (actionArray[0].length > 0) {
          actionArray[0].forEach((elem) => {
            promiseStore.push(values.updateOne({'_id': elem._id}, {$set: elem}))
          });
          return Promise.all(promiseStore)
            .then(() => server.write(mode + ' обработаны (' + actionArray[0].length + ')\n'))
        } else {
          return true
        }
      })
      .then(() => {
        if (actionArray[1].length > 0) {
          return values.insertMany(actionArray[1])
            .then(() => server.write(mode + ' добавлены (' + actionArray[1].length + ')\n'))
        } else {
          return true
        }
      })
      //.then(() => actionArray[2])
      .catch((err) => {
        throw err
      })

  } else {

    throw errMsg

  }

};

exports.value_serializer = data => {

  let arrayOfResults = [[], []],
      arrayToResult  = [],
      promises       = [],
      sort           = 0;

  const existingIds = data.self.ops.get_all_values_ids(data.update);

  function pushTemplate(countType, bool, elem) {

    const commingArray = data.self.ops.get_templates(data, elem, countType, bool, existingIds)

    if (!bool)
      sort++

    return commingArray

  }

  data.values.forEach((elem, count) => {

    promises.push(new Promise((resolve, reject) => {

      let countType = (existingIds[elem.id]) ? count : sort;

      arrayToResult = pushTemplate(countType, existingIds[elem.id] !== undefined, elem)

      const arrayType = (existingIds[elem.id]) ? 0 : 1;

      // TODO: make universal promise push. Now its only for photos

      if (arrayToResult[1].length > 0) {
        return Promise.all(arrayToResult[1])
          .then((photos) => {
            arrayToResult[0].photos = photos;
            arrayOfResults[arrayType].push(arrayToResult[0])
            resolve(arrayOfResults[arrayType])
          });
      }

      else {
        arrayOfResults[arrayType].push(arrayToResult[0])
        resolve(arrayOfResults[arrayType])
      }

    }))

  });

  return Promise.all(promises)
    .then(() => {
      return [arrayOfResults[0], arrayOfResults[1]]
    })
};

exports.ops = {

  get_templates: function (data, elem, count, isUpdating, existingIds) {

    let type = (elem.type) ? elem.type : (elem.valueType) ? elem.valueType : (existingIds[elem.id]) ? existingIds[elem.id].valueType : undefined;

    const constructor = function (localElem) {

      const self = this;

      let rules;

      if (data.mode === 'values')
        rules = ( data.self.config.rules[type] ) ? data.self.config.rules[type](elem, count, localElem)
          : data.self.config.rules['DEFAULT_TEMPLATE'](elem, count, localElem);
      else
        rules = data.self.config.rules['DEFAULT_GOODS'](elem, count, localElem);

      Object.keys(localElem).forEach((key) => {

        let rule = (rules[key] !== undefined) ? rules[key] : rules.DEFAULT;

        if (rules[key] === undefined)
          rule[0] = key;

        if (rule[2](localElem, key))
          self[rule[0]] = (typeof rule[1] === 'function') ? rule[1](data, count) : localElem[key]

      });

      Object.keys(rules).forEach((key) => {

        if (key === 'DEFAULT')
          return;

        let rule = (rules[key] !== undefined) ? rules[key] : rules.DEFAULT;

        if (rules[key] === undefined)
          rule[0] = key;

        if (self[rule[0]] === undefined && !isUpdating) {
          if (rule[1] && rule[2](localElem, key))
            self[rule[0]] = (typeof rule[1] !== 'function') ? rule[1] : rule[1](data, count);
        } else if (self[rule[0]] === undefined && isUpdating) {
          const reverseElem = data.self.config.rules['DEFAULT_REVERSE'](existingIds[localElem.id]);
          if (rule[1] && rule[2](reverseElem, key))
            self[rule[0]] = (reverseElem[key]) ? reverseElem[key] : (typeof rule[1] !== 'function') ? rule[1] : rule[1](data, count);
        }

      })

    };

    let constructedObject         = new constructor(elem),
        constructedObjectPromises = [];

    if (data.mode === 'goods' && constructedObject.photos !== undefined) {
      constructedObjectPromises = constructedObject.photos
    }

    return [constructedObject, constructedObjectPromises]

  },

  get_all_values_ids: function (documents) {

    let ids = {};

    documents.forEach(function (e) {

      if (e._id !== undefined)
        ids[e._id] = e;
      else
        ids[e.id] = e

    });

    return ids

  },

  get_values_array: function (data, key, elem) {

    const updatingValue  = elem,
          existedValueId = data.self.ops.get_all_values_ids(data.update)[updatingValue.id],
          commingValues  = data.self.ops.get_all_values_ids(updatingValue.values);

    const objectConstructor = function (elem, counter) {
      const selfEach = this;
      Object.keys(elem).forEach(function (d) {
        switch (d) {
          case 'id':
            selfEach._id = elem[d];
            break;
          default:
            selfEach[d] = elem[d]
        }
      });
      if (toUpdate[elem.id] === undefined) {
        selfEach.sort = (counter + sortCounter) * data.self.config.sortStep;
      }
      else {
        selfEach.sort = toUpdate[elem.id].sort
      }
    };

    let sortCounter = 0, result = [], toUpdate = {};

    if (existedValueId)
      existedValueId.values.forEach((elem) => {
        if (commingValues[elem._id]) {
          toUpdate[elem._id] = elem;
        } else {
          result.push(elem);
          sortCounter++
        }
      });

    updatingValue.values.forEach((elem, counter) => {
      result.push(new objectConstructor(elem, counter))
    });

    return result

  },

  add_photos: function (id, photos = [], server) {

    let result   = [],
        promises = [];

    const cloudinary = require('cloudinary');

    function callbackCatch(e, callback) {
      return callback(cloudinary.uploader.upload(__base + '/buffer/import_1c/photos/' + e.path,
        (res => {
          if (!res.error)
            return res
          else
            throw res.error
        }), {
          public_id: e.pos,
          folder: id
        }
        )
      )
    }

    cloudinary.config(require(__base + '/configs/cloudinary'));

    photos.forEach(function (e, i) {
      callbackCatch(e, function (res) {
        const promise = new Promise((resolve, reject) => {

          resolve(res)

        });

        result.push(promise);

      });
    });

    return result

  }
}
;
