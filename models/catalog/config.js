/**
 * Created by kizob on 12.05.17.
 */

exports.json = {

  catalogCollection: 'contentStores',

  catalogValuesCollection: 'catalogValues',

  catalogGoodsCollection: 'catalogElems',

  catalogModel: {
    '_id': 'catalog',
    values_id: 'catalogValues'
  },

  rules: { // TODO: make template to default keys. Maybe =)

    // [keyName, emptyValue, ruleToSet, spesialFunc]

    'checkbox-true-false': ((data, count, elem) => {
      return {
        DEFAULT: [null, 'Не задано', (() => true)],
        id: ['_id', null, (() => true)],
        name: ['name', 'Не задано',
          ((elem, key) => elem[key] !== undefined)],
        type: ['valueType', 'select',
          ((elem, key) => elem[key] !== undefined)],
        sort: ['sort', ((data, count) => (count + data.colCount) * data.self.config.sortStep),
          ((elem, key) => elem[key] === undefined)],
        values: ['values', [{
          id: 'YES',
          name: 'Да'
        }],
          ((elem, key) => elem[key] === undefined)]
      }
    }),

    'DEFAULT_GOODS': ((data, count, elem) => {
      return {
        DEFAULT: [null, 'Не задано 6543', (() => true)],
        id: ['_id', null, (() => true)],
        photos: ['photos', ((data) => {
          try {
            if (elem.photos) {
              return data.self.ops.add_photos(elem.id, elem.photos, data.server)
            }
            else {
              return 'Фотографий нет'
            }
          } catch (e) {
            //console.log(elem)
          }
        }),
          ((elem, key) => elem[key] !== undefined),
          'Promise'],
        sort: ['sort', ((data, count) => (count + data.colCount) * data.self.config.sortStep),
          ((elem, key) => elem[key] === undefined)],
        values: ['values', 'Cвойства не заданы', (() => true)]
      }
    }),

    'DEFAULT_TEMPLATE': ((data, count, elem) => {
      return {
        DEFAULT: [null, 'Не задано', (() => true)],
        id: ['_id', null, (() => true)],
        name: ['name', 'Не задано',
          ((elem, key) => elem[key] !== undefined)],
        type: ['valueType', 'select',
          ((elem, key) => elem[key] !== undefined)],
        sort: ['sort', ((data, count) => (count + data.colCount) * data.self.config.sortStep),
          ((elem, key) => elem[key] === undefined)],
        values: ['values', ((data, count) => {
          if (elem.values) {
            return data.self.ops.get_values_array(data, count, elem)
          }
          else {
            return []
          }
        }), (() => true)]
      }
    }),

    'DEFAULT_REVERSE': ((commingObject) => {
      return new (function (elem) {

        const self = this;

        Object.keys(elem).forEach((key) => {

          if (key === '_id') {
            self['id'] = elem[key];
          } else if (key === 'valueType') {
            self['type'] = elem[key];
          } else {
            self[key] = elem[key];
          }

        })
      })(commingObject)
    })
  },

  initValues: [
    {
      id: 'directory',
      type: 'directory',
      name: 'Свойства разделов'
    },
    {
      id: 'sizes',
      type: 'sizes',
      name: 'Размеры'
    },
    {
      id: 'is_complect',
      type: 'checkbox-true-false',
      name: 'Комплект'
    },
    {
      id: 'prices',
      type: 'by-size',
      name: 'Цены'
    },
    {
      id: 'amounts',
      type: 'by-size',
      name: 'Остатки'
    },
    {
      id: 'together',
      type: 'choose-id',
      name: 'Разновидность модели'
    },
    {
      id: 'buythelook',
      type: 'choose-id',
      name: 'Товары на фотографии'
    },
    {
      id: 'complect',
      type: 'choose-id',
      name: 'Товары в комплекте с изделием'
    }
  ],

  sortStep: 50

}