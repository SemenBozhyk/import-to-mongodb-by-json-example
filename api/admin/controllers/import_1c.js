exports.do_import = function(req, res) {
  res.write('Подключение совершено\n')
  try {

    var importObject = require(__base + '/buffer/import_1c/import');
    var importModule = require(__base + '/modules/import_1c/import_1c');

    res.write('Файл на месте. Начинаю импорт\n')

    importModule(importObject, res);
  }
  catch (e) {
    res.status(500)
    console.log(e)
    res.write('Файл отсутсвует или '+e+' \n')
    res.end('Закрыл соединение');
  }
};

exports.chpu_test = function(req, res) {

  var chpu = require(__base + '/commons/chpu');

  res.end(chpu(req.body.str))

};