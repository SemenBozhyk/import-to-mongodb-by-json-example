module.exports = function(app) {
  var import_1c = require('../controllers/import_1c');

  app.route('/admin/import-1c')
    .post(import_1c.do_import);

  app.route('/admin/chpu')
    .post(import_1c.chpu_test)

};