# Example of json import to mongoDB #

This repository contains example of json import to mongoDB
*(json file has special template. It's not a framework)*

## How to use: ##

```
#!npm

npm i

npm run dev
```


**Make POST request to http://localhost:3000/admin/import-1c** (I'm using Postman)

Check your mongoDB **database called "jsonImport"**



### *Some info:* ###



* *Json file placed in /buffer/import-1c/import.js*

* You can load some photos to cloudinary by adding actual api keys to /configs/cloudinary.js. Use 
*commented template in /buffer/import-1c/import.js in "goods" key*

* *There are some logs from backend on russian.*