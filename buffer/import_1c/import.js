module.exports = {
  date: "05.05.2017 02:50:55",
  values: [
    {
      id: "directory",
      values: [
        {
          id: '1с0',
          name: 'Спортивные костюмы'
        },
        {
          id: '1с1',
          name: 'Штаны'
        },
        {
          id: '1с2',
          name: 'Кофты'
        }
      ]
    },
    {
      id: "sizes",
      values: [
        {
          id: 42,
          eu: 'S'
        },
        {
          id: 44,
          eu: 'M'
        },
        {
          id: 46,
          eu: 'L'
        },
        {
          id: 48,
          eu: 'XL'
        }
      ]
    },
    {
      id: "prices",
      values: [
        {
          id: '1с0',
          name: 'Розница'
        },
        {
          id: '1с1',
          name: 'Опт'
        }
      ]
    },
    {
      id: "amounts",
      values: [
        {
          id: '1с0',
          name: 'Готовая продукция'
        },
        {
          id: '1с1',
          name: 'Крой'
        }
      ]
    },
    {
      id: "together",
      values: [
        {
          id: 'together_12345',
        }
      ]
    },
    {
      id: "buythelook",
      values: [
        {
          id: 'buy_12345',
        }
      ]
    },
    {
      id: "000",
      type: "select",
      name: "Что угодно",
      values: [
        {
          id: '1с0',
          name: 'Спортивные костюмы'
        },
        {
          id: '1с1',
          name: 'Штанs'
        },
        {
          id: '1с2',
          name: 'Кофты'
        }
      ]
    }
  ],
  goods: [
    {
      id: "g000000002101",
      // photos: [
      //   {
      //     path: "0.jpg",
      //     pos: 0
      //   },
      //   {
      //     path: "1.jpg",
      //     pos: 1
      //   },
      //   {
      //     path: "2.jpg",
      //     pos: 2
      //   }
      // ],
      values: [
        {
          id: "directory",
          values: [
            "1c1"
          ]
        },
        {
          id: "sizes",
          values: [
            42, 44, 46, 48
          ]
        },
        {
          id: "prices",
          values: [
            {
              id: "1с0",
              values: [
                [42, 1000],
                [44, 1000],
                [46, 1000],
                [48, 1000]
              ]
            },
            {
              id: "1с1",
              values: [
                [42, 605],
                [44, 650],
                [46, 750],
                [48, 650]
              ]
            }
          ]
        },
        {
          id: "amounts",
          values: [
            {
              id: "1с0",
              values: [
                [42, 1],
                [44, 0],
                [46, 0],
                [48, 3]
              ]
            },
            {
              id: "1с1",
              values: [
                [42, 1],
                [44, 2],
                [46, 3],
                [48, 4]
              ]
            }
          ]
        },
        {
          id: "together",
          values: [
            "together_12345"
          ]
        },
        {
          id: "buythelook",
          values: [
            "buy_12345"
          ]
        },
        {
          id: "000",
          values: [
            "1с0"
          ]
        }
      ]
    }
  ]
}