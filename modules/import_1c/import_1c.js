/**
 * Created by kizob on 05.05.17.
 */

var catalogMethods = require(__base + '/models/catalog');

var MongoClient = require('mongodb').MongoClient
  , assert      = require('assert');

var url = 'mongodb://localhost:27017/jsonImport';

function errHandler(data) {
  if (data.err) {

    data.server.write(data.errMessage);
    data.server.end();
    data.db.close()

  } else if (data.end) {

    var time = Number(Math.abs(data.end.end.getTime() - data.end.start.getTime())) / 1000 + "c";

    data.server.write('Импорт закончен за: ' + time + '\n');
    data.server.end('Закрыл соединение')

  } else {

    return true

  }
}

const importReturn = function (obj, server) {

  const begin       = new Date(),
        valuesArray = obj.values,
        goodsArray  = obj.goods;

  let db;

  MongoClient.connect(url)
    .then(DB => {
      db = DB;
      return catalogMethods.init_catalog(db, server)
    })
    .then(() => catalogMethods.set(db, 'values', valuesArray, server))
    .then(() => catalogMethods.set(db, 'goods', goodsArray, server))
    .then(() => {
      let time = Math.abs(new Date().getTime() - begin.getTime()) / 1000 + "c";
      server.write('Импорт закончен за: ' + time + '\n');
      return server.end('Соединение закрыто')
    })
    .catch((err) => server.end(err));

  // catalogMethods.init_catalog(db, server, function (params = {}) {
  //
  //   if (errHandler({
  //       server: server,
  //       db: db,
  //       errMessage: 'что-то пошло не так при выборе каталога\n' + params.message
  //     })) {
  //
  //     server.write('Начинаю обработку файла импорта\n')
  //
  //     catalogMethods.set('Values', db, params, obj.values, server, function (params = {}) {
  //
  //       if (errHandler({
  //           server: server,
  //           db: db,
  //           errMessage: 'что-то пошло не так при обработке свойств\n' + params.message + '\n'
  //         })) {
  //
  //         catalogMethods.set('Goods', db, params, obj.goods, server, function (params = {}) {
  //           (errHandler({
  //             end: {
  //               start: begin,
  //               end: new Date()
  //             },
  //             server: server,
  //             db: db,
  //             errMessage: 'что-то пошло не так при обработке товаров\n' + params.message + '\n'
  //           }))
  //         })
  //
  //       }
  //
  //     })
  //
  //   }
  //
  // });
  //});
};

module.exports = importReturn;